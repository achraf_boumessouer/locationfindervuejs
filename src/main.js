// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import * as VueGoogleMaps from "vue2-google-maps"
import Toastr from 'vue-semantic-ui-toastr'
Vue.config.productionTip = false
// Vue.use(VueGoogleMaps, {
//   load: {
//       key: 'AIzaSyAW0FlOWDg1vtzZaYSLSvVKJaYCRHdp1rs'
//   }
// })
/* eslint-disable no-new */
Vue.use(Toastr, {
  duration: 3000,
  container: '.toastr-container',
  autoshow: true,
  html: false,
  position: 'right top'
})
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
